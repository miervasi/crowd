from rest_framework import serializers
from movie.models import Movie
from movie.utils import write_roman

ZERO = 0


class MovieSerializer(serializers.HyperlinkedModelSerializer):
    release_roman_year = serializers.SerializerMethodField()

    class Meta:
        model = Movie
        fields = ('title', 'producers', 'directors', 'castings', 'release_roman_year')

    def get_release_roman_year(self, obj):
        return '%s' % write_roman(int(obj.release_year or ZERO))
