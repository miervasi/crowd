# Generated by Django 2.0.5 on 2018-05-13 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='castings',
            field=models.ManyToManyField(related_name='cast_set', to='person.Person'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='directors',
            field=models.ManyToManyField(related_name='director_set', to='person.Person'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='producers',
            field=models.ManyToManyField(related_name='producer_set', to='person.Person'),
        ),
    ]
