from django.db import models
from person.models import Person


class Movie(models.Model):
    producers = models.ManyToManyField(Person, related_name='producer_set')
    directors = models.ManyToManyField(Person, related_name='director_set')
    castings = models.ManyToManyField(Person, related_name='cast_set')
    release_year = models.CharField(max_length=4, null=True)
    title = models.CharField(max_length=128, null=True)

    def __str__(self):
        return '%s' % (self.title)
