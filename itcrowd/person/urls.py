
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from person.views import PersonViewSet

router = routers.DefaultRouter()
router.register(r'person', PersonViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
