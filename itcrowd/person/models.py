# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

FEMALE = 'Female'
MALE = 'Male'
OTHER = 'Other'
GENDERS = ((FEMALE, 'Mujer'), (MALE, 'Hombre'), (OTHER, 'Otro'))


class Person(models.Model):
    birthdate = models.DateField(null=True)
    gender = models.CharField(max_length=8, choices=GENDERS, null=True)
    last_name = models.CharField(max_length=128, null=True)
    first_name = models.CharField(max_length=128, null=True)
    # TODO: should be more than one alias
    alias = models.CharField(max_length=128, null=True)

    def __str__(self):
        return '%s %s' % (self.last_name, self.first_name)
