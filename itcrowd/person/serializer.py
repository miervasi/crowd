from rest_framework import serializers
from person.models import Person


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person
        fields = ('birthdate', 'last_name', 'first_name', 'alias', 'cast_set', 'producer_set', 'director_set')
