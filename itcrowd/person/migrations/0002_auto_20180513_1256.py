# Generated by Django 2.0.5 on 2018-05-13 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='gender',
            field=models.CharField(choices=[('Female', 'Mujer'), ('Male', 'Hombre'), ('Other', 'Otro')], max_length=8, null=True),
        ),
    ]
